export default {
  getMetrics () {
    return new Promise((resolve, reject) => {
      const response = require('./data/metrics.json')
      resolve(response.body)
    })
  }
}
