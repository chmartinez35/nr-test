import Vue from 'vue'
import Vuex from 'vuex'
import api from '../services/api'

Vue.use(Vuex)
Vue.config.debug = true

export const state = {
  listEditMode: false,
  activeMetric: {},
  metrics: []
}

export const getters = {
  isEditModeOn: state => {
    return state.listEditMode
  },

  isActiveMetric: (state, getters) => (metric) => {
    return state.activeMetric === metric
  },

  getMetricsList: state => {
    return state.metrics
  }
}

export const actions = {
  toggleEditMode ({ commit }) {
    commit('TOGGLE_MODE')
  },

  setActiveMetric ({ commit }, metric) {
    commit('ACTIVE_METRIC', metric)
  },

  addMetric ({ commit }, metric) {
    commit('ADD_METRIC', metric)
  },

  deleteMetric ({ commit }, metric) {
    commit('REMOVE_METRIC', metric)
  },

  editMetric ({ commit }, { metric, name }) {
    commit('EDIT_METRIC', { metric, name })
  },

  getMetrics ({ commit }) {
    api.getMetrics()
      .then(metrics => {
        commit('NEW_METRICS', metrics)
      })
      .catch(err => console.log(err))
  }
}

export const mutations = {
  TOGGLE_MODE (state) {
    state.listEditMode = !state.listEditMode
  },

  ACTIVE_METRIC (state, metric) {
    state.activeMetric = metric
  },

  ADD_METRIC (state, metric) {
    state.metrics.push({
      name: metric.name,
      metadata: metric.metadata
    })
  },

  REMOVE_METRIC (state, metric) {
    state.metrics.splice(state.metrics.indexOf(metric), 1)
  },

  EDIT_METRIC (state, { metric, name }) {
    metric.name = name
  },

  NEW_METRICS (state, metrics) {
    state.metrics = metrics
  }
}

export default new Vuex.Store({
  state,

  getters,

  actions,

  mutations,

  strict: true
})
