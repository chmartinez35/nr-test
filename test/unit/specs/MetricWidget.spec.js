import avoriaz, { mount } from 'avoriaz'
import { expect } from 'chai'
import Vuex from 'vuex'
import MetricWidget from '@/components/MetricWidget'
import MetricList from '@/components/MetricList'
import MetricToolbar from '@/components/MetricToolbar'

avoriaz.use(Vuex)

describe('MetricWidget.vue', () => {
  let state
  let actions
  let getters
  let store
  beforeEach(() => {
    actions = {
      getMetrics: sinon.stub(),
      setActiveMetric: sinon.stub(),
      deleteMetric: sinon.stub(),
      editMetric: sinon.stub(),
      toggleEditMode: sinon.stub()
    }

    getters = {
      isEditModeOn: () => false,
      isActiveMetric: (state, getters) => (metric) => false,
      getMetricsList: () => []
    }

    store = new Vuex.Store({
      state,
      getters,
      actions
    })
  })

  it('should render a MetricList', () => {
    const MetricWidgetWrapper = mount(MetricWidget, { store })
    const list = MetricWidgetWrapper.find(MetricList)
    expect(list).to.not.be.undefined
  })

  it('should render a MetricToolbar', () => {
    const MetricWidgetWrapper = mount(MetricWidget, { store })
    const toolbar = MetricWidgetWrapper.find(MetricToolbar)
    expect(toolbar).to.not.be.undefined
  })
})
