import avoriaz, { mount } from 'avoriaz'
import sinon from 'sinon'
import { expect } from 'chai'
import Vuex from 'vuex'
import MetricToolbar from '@/components/MetricToolbar'

avoriaz.use(Vuex)

describe('MetricToolbar.vue', () => {
  let state
  let actions
  let getters
  let store
  beforeEach(() => {
    actions = {
      toggleEditMode: sinon.stub()
    }

    getters = {
      isEditModeOn: () => false
    }

    store = new Vuex.Store({
      state,
      getters,
      actions
    })
  })

  it('should dispatch a toogle edit mode action when clicking on the edit-mode-toggler', () => {
    const metricToolbarWrapper = mount(MetricToolbar, { store })
    const togglerButton = metricToolbarWrapper.find('.edit-mode-toggler')[0]
    togglerButton.simulate('click')
    expect(actions.toggleEditMode.calledOnce).to.equal(true)
  })
})
