import avoriaz, { mount } from 'avoriaz'
import sinon from 'sinon'
import { expect } from 'chai'
import Vuex from 'vuex'
import MetricItem from '@/components/MetricItem'

avoriaz.use(Vuex)

describe('MetricItem.vue', () => {
  let state
  let actions
  let getters
  let store
  let fakeMetric = {
    name: 'Fake 1',
    metadata: ['Data 1', 'Data 2', 'Data 3']
  }

  beforeEach(() => {
    actions = {
      setActiveMetric: sinon.stub(),
      deleteMetric: sinon.stub(),
      editMetric: sinon.stub()
    }

    getters = {
      isEditModeOn: () => false,
      isActiveMetric: (state, getters) => (metric) => false
    }

    store = new Vuex.Store({
      state,
      getters,
      actions
    })
  })

  it('should render the metric name', () => {
    const MetricItemWrapper = mount(MetricItem, {
      store,
      propsData: {
        metric: fakeMetric
      }
    })
    expect(MetricItemWrapper.find('.name label')[0].text()).to.equal(fakeMetric.name)
  })

  it('should render the metric metadata list items', () => {
    const MetricItemWrapper = mount(MetricItem, {
      store,
      propsData: {
        metric: fakeMetric
      }
    })
    expect(MetricItemWrapper.find('.metadata-list .item').length).to.equal(fakeMetric.metadata.length)
  })

  it('should allow select the metric when edit mode is on', () => {
    // pre requisite: Edit mode getter must return "true"
    getters.isEditModeOn = () => true
    store = new Vuex.Store({
      state,
      getters,
      actions
    })

    const MetricItemWrapper = mount(MetricItem, {
      store,
      propsData: {
        metric: fakeMetric
      }
    })

    const metricItemSelector = MetricItemWrapper.find('.name')[0]
    metricItemSelector.simulate('click')
    expect(actions.setActiveMetric.calledOnce).to.equal(true)
  })
})
