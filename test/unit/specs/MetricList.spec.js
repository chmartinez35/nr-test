import avoriaz, { mount } from 'avoriaz'
import sinon from 'sinon'
import { expect } from 'chai'
import Vuex from 'vuex'
import MetricList from '@/components/MetricList'
import MetricItem from '@/components/MetricItem'
import MetricCreator from '@/components/MetricCreator'
avoriaz.use(Vuex)

describe('MetricList.vue', () => {
  let state
  let actions
  let getters
  let store
  let fakeMetrics = [{
    name: 'Fake 1',
    metadata: []
  }]

  beforeEach(() => {
    actions = {
      getMetrics: sinon.stub(),
      setActiveMetric: sinon.stub(),
      deleteMetric: sinon.stub(),
      editMetric: sinon.stub()
    }

    getters = {
      isEditModeOn: () => false,
      isActiveMetric: (state, getters) => (metric) => false,
      getMetricsList: () => fakeMetrics
    }

    store = new Vuex.Store({
      state,
      getters,
      actions
    })
  })

  it('should request for stored metrics at startup', () => {
    const MetricListWrapper = mount(MetricList, { store })
    expect(actions.getMetrics.calledOnce).to.equal(true)
    expect(MetricListWrapper.vm.getMetricsList).to.equal(fakeMetrics)
  })

  it('should render as much MetricItems as metrics retrieved by the store', () => {
    const MetricListWrapper = mount(MetricList, { store })
    const MetricItems = MetricListWrapper.find(MetricItem)
    expect(MetricListWrapper.vm.getMetricsList).to.equal(fakeMetrics)
    expect(MetricItems.length).to.equal(fakeMetrics.length)
  })

  it('should render a MetricCreator', () => {
    const MetricListWrapper = mount(MetricList, { store })
    const metricCreator = MetricListWrapper.find(MetricCreator)
    expect(metricCreator).to.not.be.undefined
  })
})
