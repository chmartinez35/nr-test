import { mutations } from '@/vuex/store'

// destructure assign mutations
const {
  ADD_METRIC,
  REMOVE_METRIC,
  EDIT_METRIC,
  TOGGLE_MODE } = mutations

describe('Vuex store Mutations', () => {
  it('should add a metric', () => {
    // mock state
    const state = {
      metrics: []
    }
    // fake metric
    const fakeMetric = {
      name: 'fake',
      metadata: []
    }
    // apply mutation
    ADD_METRIC(state, fakeMetric)
    // assert result
    expect(state.metrics.length).to.equal(1)
  })

  it('should remove a metric', () => {
    // fake metric
    const fakeMetric = {
      name: 'fake',
      metadata: []
    }
    // mock state
    const state = {
      metrics: [ fakeMetric ]
    }

    // apply mutation
    REMOVE_METRIC(state, fakeMetric)
    // assert result
    expect(state.metrics.length).to.equal(0)
  })

  it('should edit a metric name', () => {
    // fake metric
    const fakeMetric = {
      name: 'fake'
    }

    // mock state
    const state = {
      metrics: [ fakeMetric ]
    }

    // new fake name
    const newFakeName = 'new name'

    // apply mutation
    EDIT_METRIC(state, {
      metric: fakeMetric,
      name: newFakeName
    })

    // assert result
    expect(state.metrics.length).to.equal(1)
    expect(fakeMetric.name).to.equal(newFakeName)
  })

  it('should toggle the edit mode', () => {
    // mock state
    const state = {
      listEditMode: true
    }

    // apply mutation
    TOGGLE_MODE(state)

    // assert result
    expect(state.listEditMode).to.equal(false)
  })
})
