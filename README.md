# Twilio Metric Widget

> New Relic Test made by Christian Martinez 

Twilio Metric Widget (TMW) is a simple metric widget that allows to add/edit/remove metrics related to Twilio.


#TL;DR;

Run the app in development mode by following two simple steps:

``` bash
1. Install dependencies
npm install

2. Serve with hot reload at localhost:8080
npm run dev

3. (Optional) Run unit tests
npm run unit
```

Or... if you're like me and you love to see things right away, just go to <http://nr-test.netlify.com> and see it for yourself.

## Table of Contents
1. [Code Organization](#code-organization)
1. [App](#app)
1. [Tests](#tests)
1. [Notes](#notes)
1. [Recap](#recap)


## Code Organization

For this test, I decided to use a couple of frameworks/libs to develop small components in a fast and secure way. This is the stack I used:

* vue-cli for project setup
* Vue.js 2.0 combined with Vuex 2.0
* Sass for styling
* Neat to build a small Sass grid (<http://neat.bourbon.io/>).
* Breakpoint Sass for sass breakpoints (<http://breakpoint-sass.com/>);
* webpack for building, linting, pre/post css/js processing, etc.


The project uses Webpack to exploit all the capabilities offered by Vue and Vuex. All the webpack configuration can be found in the `build` and `config` folders. 

All the code related to the test implementation is located under the `src` folder. In this case, I also added a couple of tests as well, but those are located under the `test` folder.

## App

The main idea while building the client was to build scalable and maintainable components, so in order to create the system I used Vue.js (using Js ES6 compliant code) for the JS/HTML part and Sass for styling.

The project follows the organization defined by Vue.js, so in this 'src' folder we have a `main.js` script that contains the 'bootstraping' code for the vue app. This script loads a Vue instance, passing a couple of attributes, including an `App` component (explained later) and once this 'main script' is executed, all the components defined inside the 'App' component will be uploaded and ready to used. All these components are stored insinde the `components`folder. Also, I have an `assets` folder for Sass files and a `vuex` folder that contains the Vuex.js logic.


### Components

The 'components' folder contains all the components defined for the test. 
Thanks to the vue-loader, I'm able to code both html and js all in a single file. The App component located under `src` folder is the "container" used for the rest of the components.
I separated the app into four components:

- MetricWidget
- MetricToolbar
- MetricList
- MetricCreator
- MetricItem


### Vuex

The 'vuex' folder contains all the logic related to Vuex (a sort of "Redux/Flux" implementation for Vue.).
The main state object stored are the 'metrics' list and the 'activeMetric'. Both objects were used by the components to implement the desired interaction.

### Services

The 'services' folder contains an small 'api' component that decouples the metrics gathering from the Vuex store. It has a method `getMetrics` that returns a promise to the Vuex store. I've done that because I thought of getting the data from an external api, but for timing issues, I decided to keep it simple. For now, it just reads a json file stored in the 'data' folder and retrieves its information to the Vuex store.


### Styles

The 'assets' folder contains the styles used by the application. A quick note here: although Vue.js with its vue-loader allows to put html + script + scoped css/sass inside the same component file, I decided to put all the sass code in a single place, maintaining the same 'component' structure defined for the js.

Sass was selected as the tool to preprocess styles and get the final CSS. There are two main folders:`base` and `components`. The former provides the latter with variables and mixins needed to build the app. 

I used "Neat grid" to implement a tiny grid for the metrics positioning. I also used a couple of breakpoints to handle the layout and the font size of the widget (using 'breakpoint-sass' package. Checkout `src/assets/styles/base/_grids.scss` to see the values). Although this could be used from a touch device, the styling for mobile is not completed (check the #improvements section for more info).


## Tests

This folder is really simple. It has a `unit` folder that contains all the `specs` written for the app.
All the tests were written using a combination of karma-mocha-sinon as testing tools. Also, I'm using a Vue.js testing library called "avoriaz" (<https://github.com/eddyerburgh/avoriaz/>), that helped me to mount components, simulate clicks and assert Vuex functionality.

## Notes
There is an ongoing issue with the karma phantom launcher (https://github.com/karma-runner/karma-phantomjs-launcher/issues/84) that affected my environment. Just in case that you have problems when running the tests, make sure to have the following entry in your `/etc/hosts/`  file:

```
127.0.0.1 localhost
```


Regarding image handling, I used Svg sprites to show almost all the images from the mockups. The only place I didn't use an sprite was in the MetricCreator border, due to the fact that required more work to do around the use of svg sprites for the border-style-image property. Also, I do think that using a border-style-image for that element is an overkill, but I didn't have enough time to put more effort in it.

## Recap

### What went well?

I could complete the exercise, even after a long time not writing full UI code. I also glad that I could use several tools such as Neat, Netlify and Vue.js (which for me is THE best progressive fw at this moment). I was also surprised on how easy was to write tests (specially for the vuex part), but I guess that it's all because the amazing job that the community does with these great tools.

### What were the 'not so cool parts'?

When I first looked the sketch, I instantly thought about responsive design and I spent a couple of days trying to find a lib that helped me to address that, but I forgot to realize that I only had a week and a ton of other work (that right now is paying the bills) to be done. I couldn't complete the stuff I expected.

I also struggle with the svg sprite (I don't have experience using svg's) and specially with the border-image-slice prop that I 'intended' to use on the MetricCreator component. 

### What would you improve given/more time resources?

- Store/Retrieve the metrics in/from a storage service (localStorage or even an external server).

- SVG handling: right now, the svg sprite is at my index.html and I would prefer having the svg with vue directive or something like that. 

- Responsive design: 'Neat' is cool, but I'll like to use another framework to reach the RWD that I imagined. Also, I'll move all the "desktop styling" to a different css (but in that case, I'll need to do some device detection and a conditional style logic that will require time and a lot of testing).

- UI interaction tests. I'll love to start doing TDD with mocha/jasmine from scratch. In this version, the coverage is below my expectations.

- I'll definitely do more cross-browser testing!

### Do you have any feedback on the challenge (what would you change, improve, keep…)?

I would include more stuff related to the metrics metadata. Right now, it's just a list of random stuff that does... nothing. Maybe having a way to filter metrics by a certain metadata item, having a "search" box to search by metric name, or something like that. It will 'attack' the js skills, because you'll have to use some array functions to modify the stuff being shown in the DOM.

It wouldnt hurt to also make the design a bit more defined for mobile, maybe even hide some CSS challenges that you would like to see how they get solved. Right now, every page/app needs to handle mobile resolutions and having that on mind will make the applicant to think differently about almost everything.

The rest is really good I think, I would definitely keep it as it is.